> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781-01 - Advanced Database Management

## Victoria Pratt

### Assignment 1 Requirements:

*Deliverables:*

1. Distributed Version Control Setup
2. Download and Install AMPPS
3. Entity Relationship Diagram - ERD

### A1 Database Business Rules

* The human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes: job description, length of employment, benefits, number of dependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees’ histories must be tracked. Also, include the following business rules:
    * Each employee may have one or more dependents.
    * Each employee has only one job.
    * Each job can be held by many employees.
    * Many employees may receive many benefits.
    * Many benefits may be selected by many employees (though, while they may not select any benefits—any dependents of employees may be on an employee’s plan).

* In Addition:
    * Employee: SSN, DOB, start/end dates, salary.
    * Dependent: same information as their associated employee(though, not start/end dates), date added (as dependent),type of relationship: e.g., father, mother, etc.
    * Job: title (e.g., secretary, service tech., manager, cashier, janitor, IT, etc.).
    * Benefit: name (e.g., medical, dental, long-term disability, 401k, term life insurance, etc.).
    * Plan: type (single, spouse, family), cost, election date(plans must be unique).
    * Employee history: jobs, salaries, and benefit changes, as well as who made the change and why.
    * Zero Filled data: SSN, zip codes (not phone numbers: US area codes not below 201, NJ);
    * *All* tables must include notes attribute.

#### README.md file should include the following items:

* Screenshot of AMPPS installation running
* Git commands w/ short descriptions
* ERD image
* Bitbucket repository links:
    * This assignment
    * The completed tutorial repository above (bitbucketlocations).

#### Git commands w/short descriptions:

1. git init - creates an empty Git repository or reinitializes an existing one.
2. git status - displays the state of the working directory and the staging area.
3. git add - adds a change in the working directory to the staging area.
4. git commit - used to save changes to the local repository. Only saves a new commit object in the local Git repository.
5. git push - used to upload local repository content to a remote repository. How you transfer commits from your local repository to a remote repository.
6. git pull - used to fetch and download content from a remote repository and immediately update the local repository to match that content.
7. git --all - tells the command to automatically stage files that have been modified and deleted, but new files you have not told Git about are not affected.

#### Assignment Screenshots:

*Screenshot of AMPPS running*:

![AMPPS Installation Screenshot](img/ss_ampps.png)

*Screenshot of my ERD*:

![My ERD Screenshot](img/vlp16c_ERD.png)

*Screenshot of Example 1 Query Results*:

![Example 1 Query Result Screenshot](img/a1_ex1.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/vlp16c/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*Bitbucket A1 Link:*
[A1 Bitbucket Link](https://bitbucket.org/vlp16c/lis3781/src/master/a1/ "Bitbucket A1")