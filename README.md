> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 3781-01 - Advanced Database Management

## Victoria Pratt

### LIS 3781-01 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Due January 20, 2021
    - Install AMPPS
    - Provide screenshots of installations
    - Create Bitbucket Repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide Git commands with descriptions
    - Screenshot of AMPPS running
    - Screenshot of my ERD image
    - Screenshot of Example 1 Query Results
    - Bitbucket repository links

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Due February 3, 2021
    - Screenshot of my SQL code
    - Screenshot of my populated tables

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Due February 17, 2021
    - Set up RemoteLabs Oracle Server
    - Tables and data using **only** SQL
    - Screenshots of **my** SQL code
    - Screenshots of **my** populated tables (w/in the **Oracle environment**)
    - Some of the SQL code for the required reports (I did 10)
    - Bitbucket lis3781 Repository Link

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Due March 17, 2021
    - MS SQL Server
    - ERD
    - Required Reports
    - Bitbucket Repository Link

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Due March 31, 2021
    - Using MS SQL Server
    - ERD Screenshot
    - Screenshots of SQL Statement Questions

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Due March 3, 2021
    - Using MySQL Server
    - Screenshot of **my** ERD
    - Screenshot(s) of SQL Statement Questions
    - Included screenshot of successfully forward engineering to CCI server
    - lis3781 Bitbucket Repository Link

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Due April 14, 2021
    - Using MongoDB
    - Screenshot of **at least one** MongoDB shell command(s)
    - Optional: JSON code for required reports (I did 10)
    - lis3781 Bitbucket Repository Link


