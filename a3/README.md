> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781-01 - Advanced Database Management

## Victoria Pratt

### Assignment 3 Requirements:

*Deliverables:*

1. RemoteLabs Oracle Server
2. Tables and Data - Using **only** SQL
3. SQL Solutions (below)
4. **No** credit will be given if tables **and** data are not populated in RemoteLabs **Oracle** Server!

#### README.md file should include the following items:

* Screenshots of **my** SQL code
* Screenshots of **my** populated tables (w/in the **Oracle environment**)
* SQL code for some of the required reports (I did 10)
* Bitbucket repository link: **My** lis3781 Bitbucket repository link **not** Assignment 3 Bitbucket repository link

#### Assignment Screenshots:

*Screenshots of my SQL code:*

![SQL Code Screenshot(1)](img/a3_sqlcode1.png)
![SQL Code Screenshot(2)](img/a3_sqlcode2.png)
![SQL Code Screenshot(3)](img/a3_sqlcode3.png)
![SQL Code Screenshot(4)](img/a3_sqlcode4.png)
![SQL Code Screenshot(5)](img/a3_sqlcode5.png)
![SQL Code Screenshot(6)](img/a3_sqlcode6.png)

*Screenshots of my populated tables (w/in the Oracle environment):*

![Customer Populated Table](img/a3_customer.png)
![Commodity Populated Table](img/a3_commodity.png)
!["Order" Populated Table](img/a3_order.png)

*Screenshots of SQL code for **some** of the required reports:*

![Question 1](img/a3_1..png)
![Question 2](img/a3_2..png)
![Question 3](img/a3_3..png)
![Question 4a](img/a3_4a..png)
![Question 4b](img/a3_4b..png)
![Question 5](img/a3_5..png)
![Question 6](img/a3_6..png)
![Question 7](img/a3_7..png)
![Question 8](img/a3_8..png)
![Question 9](img/a3_9..png)
![Question 10](img/a3_10..png)


#### Repository Links:

*Bitbucket lis3781 Repository Link:*
[lis3781 Bitbucket Repository Link](https://bitbucket.org/vlp16c/lis3781/src/master/ "Bitbucket lis3781 Repository Link")

