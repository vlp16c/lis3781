> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781-01 Advanced Database Management

## Victoria Pratt

### Assignment 2 Requirements:

*Deliverables:*

1. Tables and insert statements.
2. Include indexes and foreign key SQL statements (see below).
3. Include **my** query result sets.
4. The following tables should be created and populated with at least 5 records **both** locally and to the CCI server.
5. **No** credit will be given if tables **and** data do not forward-engineer to the CCI server.

#### README.md file should include the following items:

* Screenshot of **my** SQL code
* Screenshot of **my** populated tables

#### Assignment Screenshots:

*Screenshots of my SQL code:*

![SQL Code Screenshot 1](img/a2_sqlcode1.png)
![SQL Code Screenshot 2](img/a2_sqlcode2.png)
![SQL Code Screenshot 3](img/a2_sqlcode3.png)

*Screenshots of my populated tables:*

![Populated Company Table Screenshot](img/a2_company_table.png)
![Populated Customer Table Screenshot](img/a2_customer_table.png)

#### Tutorial Links:

*Bitbucket lis3781 - Repository Link:*
[A2 lis3781 Repository Link](https://bitbucket.org/vlp16c/lis3781/src/master/a2/ "A2 Bitbucket Link")
