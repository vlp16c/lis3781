> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781-01 Advanced Database Management

## Victoria Pratt

### Assignment 5 Requirements:

*Deliverables:*

1. ERD
2. SQL Statement Questions
3. **No** credit will be given if tables **and** data are not populated in RemoteLabs MS SQL Server!

#### README.md file should include the following items:

* Screenshot of **my** ERD
* Screenshots of SQL code for the required reports
* **My** Bitbcuket repository link

#### Business Rules

***Business Rules:***

* Expanding upon the high-volume home office supply company's data tracking requirements, the CFO requests your services again to extend the data model's functionality. The CFO has read about the capabilities of data warehousing analytics and business intelligence (BI), and is looking to develop a smaller data mart as a test platform. He is under pressure from the members of the company's board of directiors who want to review more detailed **sales** reports based upon the following measurements:
    1. Product
    2. Customer
    3. Sales representative
    4. Time (year, quarter, month, week, day, time)
    5. Location

* Furthermore, the board members want location to be expanded to include the following characteristics of location:
    1. Region
    2. State
    3. City
    4. Store

#### Assignment Screenshots:

*Screenshot of ERD:*

![ERD Screenshot](img/a5_ERD.png)

*Screenshots of required SQL code:*

![Required Report #1 Screenshot](img/a5_1.png)
![Required Report #2 Screenshot](img/a5_2.png)
![Required Report #3 Screenshot](img/a5_3.png)
![Required Report #4 Screenshot](img/a5_4.png)
![Required Report #5 Screenshot](img/a5_5.png)
![Required Report #6 Screenshot](img/a5_6.png)
![Required Report Extra Credit Screenshot](img/a5_extracredit.png)

#### Bitbucket Repository Link:

*Bitbucket Repository Link:*
[lis3781 Bitbucket Repo Link](https://bitbucket.org/vlp16c/lis3781/src/master/ "lis3781 Bitbucket Repo Link")
