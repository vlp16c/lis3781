> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781-01 Advanced Database Management

## Victoria Pratt

### Project 1 Requirements:

*Deliverables:*

1. ERD (in .mwb format, .png files will **not** be accepted)
2. SQL Statement Questions
3. **No** credit will be given if tables **and** data do not forward-engineer to the CCI server!

### Project 1 Business Rules:

* **Business Rules:**
* As the lead DBA for a local municipality, you are contacted by the city council to design a database in order to track and document the city's court case data.
* Some report examples:
    * Which attorney is assigned to what case(s)?
    * How many unique clients have cases (be sure to add a client to more than one case)?
    * How many cases has each attorney been assigned, and names of their clients (return number and names)?
    * Which type of cases does/did each client have/had and their start and end dates?
    * Which attorney is associated to which client(s), and to which case(s)?
    * Names of three judges with the most number of years in practice, include number of years.

* Also, include the following business rules:
    * An attorney is retained by (or assigned to) one or more clients, for each case.
    * A client has (or is assigned to) one or more attorneys for each case.
    * An attorney has one or more cases.
    * A client has one or more cases.
    * Each court has one or more judges adjudicating.
    * Each judge adjudicates upon exactly one court.
    * Each judge may preside over more than one case.
    * Each case that goes to court is presided over by exactly one judge.
    * **A person can have more than one phone number.**

* **Notes:**
    * Attorney data must include social security number, name, address, office phone, home phone, e-mail, start/end dates, DOB, hourly rate, years in practice, bar (may be more than one - multivalued), specialty (may be more than one - multivalued).
    * Client data must include social security number, name, address, phone, e-mail, DOB.
    * Case data must include type, description, start/end dates.
    * Court data must include name, address, phone, e-mail, URL.
    * Judge data must include **same** information as attorneys (except bar, specialty and hourly rate; instead, use salary).
    * Must track judge historical data - tenure at each court (i.e., start/end dates), and salaries.
    * Also, history will track which courts judges presided over, if they took a leave of absence, or retired.
    * **All** tables must have notes.

* **Additional Notes:**
    * Social security numbers should be unique, and must use **SHA2 hashing with salt**.
    * Entities must be included in logical layers (colored appropriately).
    * ERD **MUST** include relationships **and** cardinalities.

#### README.md file should include the following items:

* Screenshot(s) of **my** ERD
* Screenshot(s) of SQL code for the required reports
* Bitbucket repository links: **my** lis3781 Bitbucket repository link

#### Project Screenshots:

*Screenshot of **my** ERD:*

![My ERD](img/p1_ERD.png)

*Screenshot(s) of SQL Code for required reports:*

![Required Reports SQL Code 1.](img/p1_1.png)
![Required Reports SQL Code 2.](img/p1_2.png)
![Required Reports SQL Code 3.](img/p1_3.png)
![Required Reports SQL Code 4.](img/p1_4.png)
![Required Reports SQL Code 5.](img/p1_5.png)
![Required Reports SQL Code 6.](img/p1_6.png)
![Required Reports SQL Code Extra Credit](img/p1_extracredit.png)

*Screenshot of Forward Engineering to the CCI server:*

![Forward Engineered to CCI Server](img/p1_forwardeng.png)

#### Bitbucket Repository Links:

*lis3781 Bitbucket Repository Link:*
[lis3781 Bitbucket Repository Link](https://bitbucket.org/vlp16c/lis3781/src/master/ "lis 3781 Bitbucket Repository Link")
