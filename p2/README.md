> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781-01 Advanced Database Management

## Victoria Pratt

### Project 2 Requirements:

*Deliverables:*

1. Set-up MongoDB/Follow instructions for download
2. Import JSON file
3. Screenshots
4. Bitbucket Repository Link 

#### README.md file should include the following items:

* Screenshot of **at least one** MongoDB shell command(s), (e.g., show collections)
* Optional: Some of the JSON code for the required reports (I did 10)
* Bitbucket repository link: **my** lis3781 Bitbucket repository link

#### Project 2 Screenshots:

* **NOTE: I SPELLED "RESTAURANTS" WRONG, BUT DID EVERYTHING ELSE CORRECT // EVERYTHING ELSE WORKED CORRECTLY**

*Screenshot of MongoDB Shell Commands:*

![MongoDB Shell Commands](img/shellcommands.png)

*Screenshots of JSON code (10):*

![1. JSON Code](img/JSON1.png)
![2. JSON Code](img/JSON2.png)
![3. JSON Code](img/JSON3.png)
![4. JSON Code](img/JSON4.png)
![5. JSON Code](img/JSON5.png)
![6. JSON Code](img/JSON6.png)
![7. JSON Code](img/JSON7.png)
![8. JSON Code](img/JSON8.png)
![9. JSON Code](img/JSON9.png)
![10. JSON Code](img/JSON10.png)

#### Repository Link:

*Bitbucket Repository Link:*
[P2 Repository Link](https://bitbucket.org/vlp16c/lis3781/src/master/ "lis3781 Bitbucket Repository Link")
